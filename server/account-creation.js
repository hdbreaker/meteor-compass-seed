Accounts.onCreateUser(function(options, user) {
    // Use provided profile in options, or create an empty object
    user.profile = options.profile || {};
    // Assigns first and last names to the newly created user object
    user.profile.company = options.company;
    user.profile.category= options.category;
    user.profile.country= options.country;
    user.profile.city= options.city;
    user.profile.editpassword= options.editpassword;
    user.profile.languaje= options.languaje;
    user.profile.currency= options.currency;
    // Returns the user object
    return user;
});